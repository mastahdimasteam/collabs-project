$(function(){

	// slider
	$('.hero-slider.owl-carousel').owlCarousel({
		items: 1,
		nav: true,
		dot: true,
		autoplay: true,
		loop: true,
		navText :  ["<span class='fa fa-chevron-left'><span>","<span class='fa fa-chevron-right'><span>"],
		responsive: {
			992:{
				items: 1
			}
		}
	})

	// off canvas
	$('.canvas-trigger,.close-off-canvas').click(function(e){
		e.preventDefault();
		$('.off-canvas').toggleClass('open');
		$('body .inner').toggleClass('off-canvas-active');
	})

	// masonry gallery
	var masonryGallery = function(){
		$('.gallery-body').masonry({
			itemSelector: '.gallery-item',
			fitWidth: true,
			horizontalOrder: false,
			percentPosition: true,
		});
	}

	masonryGallery();

	// click filter gallery
	$('.gallery-filter li > a').click(function(e){
		
		e.preventDefault();
		// get data attribute
		var filter = $(this).data('filter');
		
		// get element to be loop
		$('.gallery-item').each(function(index, element){
			console.log(element);
			if(filter != ''){
				if($(element).data('filter') != filter){
					$(element).addClass('hidden');
				}else{
					$(element).removeClass('hidden');
				}
			}else{
				$(element).removeClass('hidden');
			}
		});

		masonryGallery();
	})

	// click view gallery
	$('.gallery-view li > a').click(function(e){
		e.preventDefault();
		var view = $(this).data('view');
		$('.gallery-item').each(function(index, element){
			if(view == 'detail'){
				$(element).css({
					width: '50%'
				})
			}else if(view == 'many'){
				$(element).css({
					width: '25%'
				})
			}
			masonryGallery();
		})
	})

});

